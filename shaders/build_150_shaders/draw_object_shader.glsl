
varying vec3 vertex_light_position;
varying vec3 vertex_normal;
uniform bool use_tex;
uniform sampler2D tex;

void main(void)
{
	float f=100.0;
	float n = 0.1;
	float z = (2. * n) / (f + n - gl_FragCoord.z * (f - n));
	
	float diffuse_value = max(dot(vertex_normal, vertex_light_position), 0.0) + .2;
	
	float alpha = sqrt(2. - z*6.);
	vec4 col;
	if(!use_tex)
	{
		col = gl_FrontMaterial.diffuse*diffuse_value;
	}
	else {
		vec2 uv = gl_TexCoord[0].st;
		col = texture2D(tex, uv);
	}
	gl_FragColor = vec4(col.xyz, gl_FrontMaterial.diffuse.w*alpha);
}
