
uniform vec2 uv_scale;
uniform vec2 uv_offset;
uniform sampler2D disp0;
uniform sampler2D disp1;
uniform float lv;
uniform float rv;
uniform float alpha;
uniform vec3 colors[8];
uniform float streams[8];
void main()
{
	vec2 uv = gl_TexCoord[0].st*uv_scale + uv_offset;
	
	int stream = 0;
	for(int i=0;streams[stream] < uv.x && stream < 8;stream++) { }
	vec4 col = vec4(colors[stream], alpha);
	
	if(col.a == 0.)
	{
		discard;
	}
	gl_FragColor = col;
}
