varying vec3 vertex_light_position;
varying vec3 vertex_normal;

void main()
{

	vertex_light_position = normalize(gl_LightSource[0].position.xyz);
	
	gl_Position = ftransform();
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vertex_normal = normalize(gl_NormalMatrix * gl_Normal);
}
