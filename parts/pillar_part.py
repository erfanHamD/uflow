try:
	from part import Part
except:
	from .part import Part
#import numpy as np
import math

def linspace(start, end, number):
	return map(lambda x: float(x)/(number-1)*(end-start)+start,range(number))

class Pillar_Part(Part):
	def __init__(self, *args, **kwargs):
		super(Pillar_Part, self).__init__(*args, **kwargs)
		self.pillar_spacing = 3
	def get_legal_args(self):
		return {"Offset": (-.5, -.375, -.25, -.125, 0, .125, .25, .375, .5), "Diameter": (.375, .5, .625, .75), "width": (1,), "height": (.25,)}
	def get_attachment_points(self):
		return ((1, (self.pillar_spacing, 0), (1,0)),)
	def gen_geometry(self):
		r = self.args['Diameter']/2.
		if abs(self.args['Offset']) + r < self.args['width']/2.:
			#Easy case: no intersection between pillar and walls
			geom = []
			geom.append(((0, self.args['width']/2.), (self.pillar_spacing, self.args['width']/2.)))
			geom.append(((0, -self.args['width']/2.), (self.pillar_spacing, -self.args['width']/2.)))
			geom.append(((math.cos(theta)*r + 1, math.sin(theta)*r - self.args['Offset']) for theta in linspace(0, 2*math.pi, 50)))
			return geom
		else:
			#Pillar intersects wall. Time for some maths.
			if self.args['Offset'] <= 0:
				#Intersect the top wall
				theta = math.asin((self.args['width']/2. + self.args['Offset'])/r)
				topwall = [(0, self.args['width']/2.)]
				topwall.extend([(math.cos(t)*r + 1, math.sin(t)*r - self.args['Offset']) for t in linspace(math.pi-theta, 2*math.pi+theta, 50)])
				topwall.extend([(self.pillar_spacing, self.args['width']/2.)])
				
				bottomwall = [(self.pillar_spacing, -self.args['width']/2.)]
				bottomwall.extend([(math.cos(t)*r + 1, math.sin(t)*r - self.args['Offset'] - self.args['width']) for t in linspace(theta, math.pi-theta, 50)])
				bottomwall.extend([(0, -self.args['width']/2.)])
				geom = [topwall, bottomwall]
				return geom
			else:
				#Intersect bottom wall
				#TBD
				theta = math.asin((-self.args['width']/2. + self.args['Offset'])/r)
				topwall = [(0, self.args['width']/2.)]
				topwall.extend([(math.cos(t)*r + 1, math.sin(t)*r + self.args['width'] - self.args['Offset']) for t in linspace(math.pi-theta, theta+2*math.pi, 50)])
				topwall.extend([(self.pillar_spacing, self.args['width']/2.)])
				
				bottomwall = [(self.pillar_spacing, -self.args['width']/2.)]
				bottomwall.extend([(math.cos(t)*r + 1, math.sin(t)*r + self.args['width'] - self.args['Offset'] - self.args['width']) for t in linspace(theta, math.pi-theta, 50)])
				bottomwall.extend([(0, -self.args['width']/2.)])
				geom = [topwall, bottomwall]
				return geom
