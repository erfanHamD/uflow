#import numpy as np

def matmult(a,b):
    zip_b = zip(*b)
    return [[sum(float(ele_a)*ele_b for ele_a, ele_b in zip(row_a, col_b)) for col_b in zip_b] for row_a in a]

class Part(object):
	def __init__(self, *args, **kwargs):
		super(Part, self).__init__(*args, **kwargs)
		self.args = dict((k,v[0]) for k,v in self.get_legal_args().iteritems())
		self.position = (0,0)
		self.direction = (1,0)
		self.attached_objects = []
		#self.M_parent = np.identity(3)
		self.M_parent = ((1,0,0), (0,1,0), (0,0,1))
		self.polylines = []
	def get_legal_args(self):
		return {"width": (1,)}
	def get_attachment_points(self):
		return ()
	def set_args(self, **args):
		legals = self.get_legal_args()
		for k,v in args.iteritems():
			legal_vs = legals.get(k, None)
			if legal_vs is None:
				return False
			else:
				try:
					ind = legal_vs.index(v)
					self.args[k] = legal_vs[ind]
				except:
					return False
	def can_attach(self, target, attachment):
		if self.get_legal_args()["width"].count(attachment[0]) > 0:
			return True
		else:
			return False
	def get_nearest_attachment(self, position):
		points = self.get_attachment_points()
		nearest = points[0]
		dist = (position[0] - nearest[1][0])**2 + (position[1] - nearest[1][1])**2
		for i in points:
			d = (i[1][0] - position[0])**2 + (i[1][1] - position[1])**2
			if d < dist:
				dist = d
				nearest = i
		return nearest
	def attach(self, target, attachment):
		if self.can_attach(target, attachment):
			self.set_args(width = attachment[0])
			self.position = attachment[1]
			self.direction = attachment[2]
			self.compute_transform_matrix()
			self.polylines = self.gen_geometry()
			self.parent = target
			target.attached_objects.append((attachment, self))
	def gen_geometry(self):
		return []
	def compute_transform_matrix(self, propagate = True):
		#P = np.asarray(((1, 0, self.position[0]), (0, 1, self.position[1]), (0, 0, 1)))
		P = ((1, 0, self.position[0]), (0, 1, self.position[1]), (0, 0, 1))
		V = self.direction
		#R = np.asarray(((V[0], -V[1], 0), (V[1], V[0], 0), (0,0,1)))
		R = ((V[0], -V[1], 0), (V[1], V[0], 0), (0,0,1))
		#self.M = P.dot(R)
		self.M = matmult(P,R)
		if propagate:
			for c in self.attached_objects:
				#c[1].M_parent = self.M_parent.dot(self.M)
				c[1].M_parent = matmult(self.M_parent, self.M)
				c[1].compute_transform_matrix()
		return self.M
	def get_transformed_polylines(self):
		self.polylines = self.gen_geometry()
		#M = self.M_parent.dot(self.compute_transform_matrix())
		M = matmult(self.M_parent, self.compute_transform_matrix())
		#new_polylines = [[M.dot((p[0], p[1], 1))[0:2] for p in polyline] for polyline in self.polylines]
		new_polylines = [[map(lambda x: x[0], matmult(M,((p[0],), (p[1],), (1,))))[0:2] for p in polyline] for polyline in self.polylines]
		return new_polylines 
	def get_bounding_box(self):
		polylines = self.get_transformed_polylines()
		left = float("Inf")
		right = float("-Inf")
		top = float("-Inf")
		bottom = float("Inf")
		
		for polyline in polylines:
			for point in polyline:
				if point[0] < left:
					left = point[0]
				if point[0] > right:
					right = point[0]
				if point[1] < bottom:
					bottom = point[1]
				if point[1] > top:
					top = point[1]
		return (left, bottom, right, top)
