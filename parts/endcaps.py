try:
	from part import Part
except:
	from .part import Part

class Start_Endcap(Part):
	def get_attachment_points(self):
		return ((1,(.5, 0),(1,0)),)
	def gen_geometry(self):
		return [[(.5, -.5), (0,-.5), (0, .5), (.5, .5)]]
		

class Endcap(Part):
	def gen_geometry(self):
		return [[(0, .5), (.5,.5), (.5, -.5), (0, -.5)]]
		
