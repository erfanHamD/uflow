# uFlow v1.0 Documentation

## Table of Contents
1. [About](#markdown-header-about)
2. [Download](#markdown-header-download)
3. [Installation](w#markdown-header-installation)
	* [Windows](#markdown-header-windows)
	* [Linux](#markdown-header-linux)
4. [Usage Notes](#markdown-header-usage-notes)
	* [The tool pane](#markdown-header-the-tool-pane)
	* [The device pane](#markdown-header-the-device-pane)
	* [The output pane](#markdown-header-the-output-preview-pane)
5. [Troubleshooting](#markdown-header-troubleshooting)
6. [Contact](#markdown-header-contact)


## About
uFlow is a graphical tool for the rational design of intertial microfluidic devices that utilize pillar geometries to deform the laminar flow field. uFlow utilizes pre-computed advection maps to enable fast, real-time feedback of flow deformation without utilizing an online Navier-Stokes solver. Find out more about the inner workings of the program in an upcoming publication.

uFlow is provided as-is without guarantee of stable operation or accuracy of results. Please bear in mind that uFlow is a work-in-progress; as such, it is expected to contain show-stopping bugs. We have, to the best of our ability, tested the program on a range of hardware and determined it to be usably stable and accurate for a limited set of use-cases. If you have questions about the accuracy of the program output, or find an error in the program's operation, or wish to speak with us for any other reason, please feel free to contact us as detailed in the [contact](https://bitbucket.org/baskargroup/uflow/overview#markdown-header-contact) section of this document.

## DOWNLOAD

The latest release version of uFlow can be downloaded from [www.biomicrofluidics.com/software.php](http://biomicrofluidics.com/software.php)

If you obtained this software from a different source, we cannot guarantee the safety of the program's execution. Please be sure that you have downloaded the software from a trustworthy source.

## INSTALLATION 

Presently, the software is shipped in a self-contained ZIP or TAR.GZ archive depending on your platform. Once this archive is extracted, no further installation steps are required. However, some platforms require further dependencies to be installed to ensure full range of software functionality. Please find your platform in the list below.

#### Windows

The Windows binary includes all required and optional dependencies. To run the program, simply double-click the uFlow.exe executable in the extracted folder.

#### Linux

Most linux distributions will include the necessary python interpreter. You will need to use your distribution-specific method of installing the relevant python libraries; beyond the standard ones, you'll need pyglet, Tkinter (optional) and PIL (optional). On Ubuntu and its derivatives,  these may be installed from the command line as follows:
```
sudo apt-get install python-pyglet python-imaging python-tk
```
You may also consider using python's library installation if you do not have administrator priveleges:
```
pip install PIL
pip install pyglet
```
You will need to make sure that libjpeg, libz, and libfreetype are available on your system.

Once the relevant dependencies have been installed, you can run the software by executing the uflow.py script from the unzipped directory. Please ensure that the working directory matches the location of the uflow.py script; this is required to allow the software to locate the data used to compute the transformations.
```
cd uFlow/
python uflow.py
```
## USAGE NOTES

Upon first run of the software, you will be greeted with a device consisting of a single empty channel and three parallel inlet flows, represented with three colored dyes. The screen is divided into three parts. From left to right, they are: (1) the tool pane, (2) the device pane, and (3) the output visualization pane.

#### THE TOOL PANE

At the top of the tool pane, the user may adjust the inlet flow conditions. This is done by sliding the triangular handles marking the boundaries between fluid streams. To add a new handle, double-click somewhere within the fluid stream. To remove a handle, drag a handle all the way to the  left or right edge of the fluid stream. To change the color of a fluid stream, right-click that stream to bring up the color palette, and select a color from the options presented. Note that adjustments to the inlet flow conditions are reflected instantaneously at the output and at all points in the flow. Click on any stream to highlight it in the device
schematic.

In the middle of the tool pane are controls for adjusting the parameters of the currently-selected pillar in the schematic. To use these controls, click on an already-placed pillar in the schematic and drag the handles. Note that the controls snap to a number of discrete points rather than having continuous operation; this is because the results have been pre-computed and cached for a finite number of pillar geometries and locations. In some cases, some tick marks will be inaccessible; this is because there is no data stored for that particular geometry configuration.

At the bottom of the tool pane are buttons for standard operation of the software. uFlow supports unlimited UNDO and REDO. These commands may also be accessed using the Ctrl+Z and Ctrl+Y hotkeys, respectively. The NEW, SAVE, and LOAD buttons can be used to store devices in the software's native format. This allows the devices to be reloaded at a later date for further work. This functionality is also bound to the Ctrl+N, Ctrl+S, and Ctrl+O hotkeys, respectively. The EXPORT DXF option exports a version of the device suitable for interoperability with other computer-aided design programs, such as AutoCAD, as well as vector graphics programs such as Illustrator.

#### THE DEVICE PANE

In the center pane, the user can interact with a schematic representation of the device. Note that the spacing between pillars in this pane is not accurate; it has been reduced to enable easier user interaction. To view an accurate physical description of the device, it is recommended to export a DXF and view the device in your tool of choice.

In this pane, green cubes represent candidate pillar positions for pillars of the currently-selected diameter. To place a pillar, position the mouse cursor over a candidate pillar position, and press the left mouse button. Once a pillar has been placed, the software automatically shifts the view downstream of the placed pillar, to facilitate placement of additional pillars. The user may adjust an already-placed pillar by positioning the mouse cursor over that pillar, and clicking on it. This selects the pillar for adjustment. That pillar may now be adjusted using the sliders in the tool pane, or by left click-dragging the pillar into a new position. The user may also change the current selection using the LEFT and RIGHT arrow keys on his or her keyboard. The UP and DOWN arrows can be used to change the current pillar diameter.

To remove a pillar, select the pillar and press the DELETE button on your keyboard.

To zoom in on the device, use the mouse scroll wheel. Right-click drag to orbit the current view about the device. Left-click drag to pan around the device. Use the C hotkey to toggle between the standard view and a top-down orthographic view of the device.


#### THE OUTPUT PREVIEW PANE

At the top of the rightmost pane of the screen is the realtime output preview. This is a low-resolution preview of the transformation at the location beneath the mouse cursor. Note that this is simply an interpolation of the transformation occuring at each boundary, and does  NOT represent the actual fluid cross-section at the mouse position. Instead, it is intended as a visual tool for understanding the steps in the fluid transformation. The actual fluid dynamics are more complex and are not captured in this preview.

Second in the output preview pane is a high-resolution view of the fluid cross-section at end of the device, after all pillars have contributed their transformation. The user may click this preview to compute a very high-resolution version of the result. If PIL is present on the system, a SAVE button will appear below the high-resolution result, allowing the user to save a PNG of this image. Note that a number of numerical issues at the boundaries of the simulations result in visual artifacts in the computed flow field. These are non-physical and unfortunately unavoidable at present. We are investigating methods for mitigating these artifacts.

In the preview versions of the software, a debug window appears at the bottom of the output preview pane. This window contains information on the state of the program, and can be useful for spotting bugs. For more information, please see the TROUBLESHOOTING portion of this document.

## TROUBLESHOOTING

uFlow requires a modern OpenGL implementation with floating-point framebuffer support, in order to perform fast computation with the GPU. This feature is supported by the majority of graphics cards manufactured since 2004. However, some Windows installations do not ship with the drivers necessary to expose this functionality. Please ensure that you have complete, up-to-date drivers installed for the graphics hardware in your computer, available from the manufacturer's website.

In certain integrated graphics hardware found in laptops, this functionality is (unfortunately) not available. If your graphics drivers are up-to-date and you still receive this error, it is possible that you have one of these chipsets. In our experience, some hardware drivers will support the relevant functionality in one OS but not another (in particular, OpenGL support on Intel chipsets appears to be better on linux than on Windows). Unfortunately, there is nothing we can do to fix this issue, and we recommend switching to another computer or installing a new graphics card.

If you find any bugs, or if your question is not answered here, please do not hesitate to contact us using the information described in the CONTACT(#contact) section of this document.

## CONTACT

If you wish to contact the author with questions, comments, bug reports, or for any other purpose, please do so at the following address:
```
Dan Stoecklein
stoeckd@iastate.edu
```
This software is being developed as part of an ongoing research effort by the DiCarlo lab at UCLA. For more information about the lab and our other work, please see our website at [biomicrofluidics.com](www.biomicrofluidics.com).
