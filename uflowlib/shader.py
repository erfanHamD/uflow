#
# Copyright Tristam Macdonald 2008.
#
# Distributed under the Boost Software License, Version 1.0
# (see http://www.boost.org/LICENSE_1_0.txt)
#
try:
    print("loading shader.py")
except:
    import sys
    sys.stdout = open('stdout.txt', 'w')
    sys.stderr = open('stderr.txt', 'w')

from pyglet.gl import *
from ctypes import *
import pyglet as pg

class Shader:
    def __init__(self, vert = [], frag = [], geom = []):
        self.handle = glCreateProgram()
        self.linked = False
        self.createShader(vert, GL_VERTEX_SHADER)
        self.createShader(frag, GL_FRAGMENT_SHADER)
        self.link()
        self.renderquad = pg.graphics.Batch()
        self.renderquad.add(4, pg.gl.GL_QUADS, None, ('v2i', (0, 0, 1, 0, 1, 1, 0, 1)), ('t2f', (0, 0, 1, 0, 1, 1, 0, 1)))
        return

    def createShader(self, strings, type):
        count = len(strings)
        strings = map(lambda x: x.encode('ascii'), strings)
        # if we have no source code, ignore this shader
        if count < 1:
            return
        else:
            # create the shader handle
            shader = glCreateShader(type)

            # convert the source strings into a ctypes pointer-to-char array, and upload them
            # this is deep, dark, dangerous black magick - don't try stuff like this at home!
            src = (c_char_p * count)(*strings)
            glShaderSource(shader, count, cast(pointer(src), POINTER(POINTER(c_char))), None)

            # compile the shader
            glCompileShader(shader)

            temp = c_int(0)
            # retrieve the compile status
            glGetShaderiv(shader, GL_COMPILE_STATUS, byref(temp))

            # if compilation failed, print the log
            if not temp:
                # retrieve the log length
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, byref(temp))
                # create a buffer for the log
                buffer = create_string_buffer(temp.value)
                # retrieve the log text
                glGetShaderInfoLog(shader, temp, None, buffer)
                # print the log to the console
                print(buffer.value)
            else:
                # all is well, so attach the shader to the program
                glAttachShader(self.handle, shader)
            return

    def link(self):
        # link the program
        glLinkProgram(self.handle)

        temp = c_int(0)
        # retrieve the link status
        glGetProgramiv(self.handle, GL_LINK_STATUS, byref(temp))

        # if linking failed, print the log
        if not temp:
            #    retrieve the log length
            glGetProgramiv(self.handle, GL_INFO_LOG_LENGTH, byref(temp))
            # create a buffer for the log
            buffer = create_string_buffer(temp.value)
            # retrieve the log text
            glGetProgramInfoLog(self.handle, temp, None, buffer)
            # print the log to the console
            print(buffer.value)
        else:
            # all is well, so we are linked
            self.linked = True
        return

    def bind(self):
        # bind the program
        glUseProgram(self.handle)

    def unbind(self):
        # unbind whatever program is currently bound - not necessarily this program,
        # so this should probably be a class method instead
        glUseProgram(0)

    # upload a floating point uniform
    # this program must be currently bound
    def uniformf(self, name, *vals):
        # check there are 1-4 values
        if len(vals) in range(1, 5):
            # select the correct function
            { 1 : glUniform1f,2 : glUniform2f,
                3 : glUniform3f,
                4 : glUniform4f
                # retrieve the uniform location, and set
            }[len(vals)](glGetUniformLocation(self.handle, name.encode('ascii')), *vals)

    # upload a floating point uniform
    # this program must be currently bound
    def uniformfv(self, name, vsize, *vals):
        # check there are 1-4 values
        if vsize in range(1, 5):
            # select the correct function
            { 1 : glUniform1fv,2 : glUniform2fv,
                3 : glUniform3fv,
                4 : glUniform4fv
                # retrieve the uniform location, and set
            }[vsize](glGetUniformLocation(self.handle, name.encode('ascii')), len(vals)/vsize, (GLfloat*(len(vals)))(*vals))

    # upload an integer uniform
    # this program must be currently bound
    def uniformi(self, name, *vals):
        # check there are 1-4 values
        if len(vals) in range(1, 5):
            # select the correct function
            { 1 : glUniform1i,
                2 : glUniform2i,
                3 : glUniform3i,
                4 : glUniform4i
                # retrieve the uniform location, and set
            }[len(vals)](glGetUniformLocation(self.handle, name.encode('ascii')), *vals)

    # upload a uniform matrix
    # works with matrices stored as lists,
    # as well as euclid matrices
    def uniform_matrixf(self, name, mat):
        # obtian the uniform location
        loc = glGetUniformLocation(self.handle, name.encode('ascii'))
        # uplaod the 4x4 floating point matrix
        glUniformMatrix4fv(loc, 1, False, (c_float * 16)(*mat))

    def draw(self):
        #Assume we're bound already
        self.renderquad.draw()
