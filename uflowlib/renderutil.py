import pyglet as pg
from pyglet.gl import *
from . import globjects as glo

FBO, Texture = glo.FBO, glo.Texture

class Cube:
    def __init__(self, l = 1, w = 1, h = 1):
        self.batch = pg.graphics.Batch()
        self.batch.add(4, GL_QUADS, None, ('v3f/static', 
          (-1 / 2, -1 / 2, -1 / 2,
          1 / 2, -1 / 2, -1 / 2,
          1 / 2, -1 / 2, 1 / 2,
          -1 / 2, -1 / 2, 1 / 2)), ('n3f', 
          (0, -1, 0,
          0, -1, 0,
          0, -1, 0,
          0, -1, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', 
          (-1 / 2, 1 / 2, -1 / 2,
          -1 / 2, 1 / 2, 1 / 2,
          1 / 2, 1 / 2, 1 / 2,
          1 / 2, 1 / 2, -1 / 2)), ('n3f', 
          (0, 1, 0,
          0, 1, 0,
          0, 1, 0,
          0, 1, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', 
          (-1 / 2, -1 / 2, -1 / 2,
          -1 / 2, -1 / 2, 1 / 2,
          -1 / 2, 1 / 2, 1 / 2,
          -1 / 2, 1 / 2, -1 / 2)), ('n3f', 
          (-1, 0, 0,
          -1, 0, 0,
          -1, 0, 0,
          -1, 0, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', 
          (1 / 2, -1 / 2, -1 / 2,
          1 / 2, 1 / 2, -1 / 2,
          1 / 2, 1 / 2, 1 / 2,
          1 / 2, -1 / 2, 1 / 2)), ('n3f', 
          (1, 0, 0,
          1, 0, 0,
          1, 0, 0,
          1, 0, 0)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', 
          (-1 / 2, -1 / 2, 1 / 2,
          1 / 2, -1 / 2, 1 / 2,
          1 / 2, 1 / 2, 1 / 2,
          -1 / 2, 1 / 2, 1 / 2)), ('n3f', 
          (0, 0, 1,
          0, 0, 1,
          0, 0, 1,
          0, 0, 1)))
        self.batch.add(4, GL_QUADS, None, ('v3f/static', 
          (-1 / 2, -1 / 2, -1 / 2,
          -1 / 2, 1 / 2, -1 / 2,
          1 / 2, 1 / 2, -1 / 2,
          1 / 2, -1 / 2, -1 / 2)), ('n3f', 
          (0, 0, -1,
          0, 0, -1,
          0, 0, -1,
          0, 0, -1)))

    def draw(self):
        self.batch.draw()

class Camera:
    def __init__(self, pos = (0,0,0), lookat = (0,1,0), up = (0,0,1), locked = False):
        self.pos = pos
        self.lookat = lookat
        self.up = up
        self.locked = locked
        self.time = 0
        self.timer = 1
        self.oldpos = pos
        self.oldla = lookat
        self.oldup = up
        self.fac = 1
        self.lerping = False
    
    def transform(self):
        pos = self.pos
        lookat = self.lookat
        up = self.up
        if self.lerping:
            fac = self.fac
            pos = [i[0]*fac + i[1]*(1-fac) for i in zip(pos, self.oldpos)]
            lookat = [i[0]*fac + i[1]*(1-fac) for i in zip(lookat, self.oldla)]
            up = [i[0]*fac + i[1]*(1-fac) for i in zip(up, self.oldup)]
        if not self.locked:
            lookat = [i[0] + i[1] for i in zip(pos,lookat)]
        gluLookAt(pos[0], pos[1], pos[2], lookat[0], lookat[1], lookat[2], up[0], up[1], up[2])
    
    def move(self, pos = None, lookat = None, up = None, locked = None, time = 0):
        if pos is None:
            pos = self.pos
        if lookat is None:
            lookat = self.lookat
        if up is None:
            up = self.up
        if self.lerping:
            fac = self.fac
            self.pos = [i[0]*fac + i[1]*(1-fac) for i in zip(self.pos, self.oldpos)]
            self.lookat = [i[0]*fac + i[1]*(1-fac) for i in zip(self.lookat, self.oldla)]
            self.up = [i[0]*fac + i[1]*(1-fac) for i in zip(self.up, self.oldup)]
        if locked is not None:
            if self.locked == False and locked == True:
                self.lookat = [i[1] + i[0] for i in zip(self.pos,self.lookat)]
            elif self.locked == True and locked == False:
                self.lookat = [i[1] - i[0] for i in zip(self.pos,self.lookat)]
            self.locked = locked
        self.oldpos = self.pos
        self.oldla = self.lookat
        self.oldup = self.up
        self.pos = pos
        self.lookat = lookat
        self.up = up
        self.timer = time
        self.time = 0
        if self.timer > 0:
            self.lerping = True
            self.fac = 0
    
    def update(self, dt):
        if self.lerping:
            self.time += dt
            if self.time >= self.timer:
                self.time = self.timer
                self.lerping = False
            self.fac = self.time/self.timer
            
class IPythonPygletWindow(object):
    def __init__(self, *args, **kwargs):
        if len(args) >= 2:
            self._dims = (args[0], args[1])
        else:
            self._dims = kwargs.get("dims", (640, 480))
        self._fbo = FBO()
        self._target = Texture(dims = self._dims, channels = 4)
    def bind(self):
        self._fbo.bind(self._target)
        glClear(GL_COLOR_BUFFER_BIT)
    def show(self):
        from IPython.display import Image
        return Image(self.write_png())
    def write_png(self):
        
        import zlib, struct
        buf = self._target.read_pixels().tostring()
        width = self._dims[0]
        height = self._dims[1]
        # reverse the vertical line order and add null bytes at the start
        width_byte_4 = width * 4
        raw_data = b''.join(b'\x00' + buf[span:span + width_byte_4]
                            for span in range((height - 1) * width * 4, -1, - width_byte_4))
    
        def png_pack(png_tag, data):
            chunk_head = png_tag + data
            return (struct.pack("!I", len(data)) +
                    chunk_head +
                    struct.pack("!I", 0xFFFFFFFF & zlib.crc32(chunk_head)))
    
        return b''.join([
            b'\x89PNG\r\n\x1a\n',
            png_pack(b'IHDR', struct.pack("!2I5B", width, height, 8, 6, 0, 0, 0)),
            png_pack(b'IDAT', zlib.compress(raw_data, 9)),
            png_pack(b'IEND', b'')])
