#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  particle_trace.py
#  
#  Copyright 2013 Keegan Owsley <keegan@ucla.edu>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from __future__ import absolute_import


import sys
PY3 = sys.version > '3'


import pyglet as pg
from pyglet.gl import *
from ctypes import byref
import ctypes
from floattex import load_floatimg
if PY3:
	import configparser as ConfigParser
else:
	import ConfigParser
import array
from shader import Shader
import numpy as np
import os.path
from . import vectormath as v
from . import globjects as gl


class ParticleTracer(object):
	def __init__(self, *args, **kwargs):
		self._xlims = kwargs.get('xlims', [0, 1])
		self._ylims = kwargs.get('ylims', [0, 1])
		
		self._vel = kwargs.get('velocity', np.zeros((2, 2, 2)))
		self._dims = self._vel.shape
		
	def step_particles(self):
		raise NotImplementedError
	

class OGLParticleTracer(ParticleTracer):
	'''A particle tracer that uses OpenGL for hardware computation.'''
	
	def make_shader(self):
		'''Construct and compile a GLSL shader that performs the 
particle trace according to the tracer's parameters.'''
		
		#TBD: implement non-3D case
		#TBD: implement chunking of velocity field 
		
		#Shader uniforms
		shader = '''#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D pos;
uniform sampler2D vel_field;
'''

		if self._affine:
			shader += "\nconst mat4 M = mat4("
			shader += ",".join(map(lambda x: "%e"%(x), np.asarray(self._M).flatten()))
			shader += ");\n"
			shader += "const mat4 Minv = mat4("
			shader += ",".join(map(lambda x: "%e"%(x), np.asarray(self._Minv).flatten()))
			shader += ");\n\n"
			
		if self._constrain_particles:
			shader += 'const float edge = ' + str(self._edge) + ";\n"
			shader += '''
float check_bounds(vec4 p)
{
''' 
			shader += "\tconst vec3 dims = vec3(%e,%e,%e);\n"%(self._xw, self._yw, self._zw)
			shader += '''
	bool outside = false;

	if(p.x < edge) { outside = true;}
	if(p.y < edge) { outside = true;}
	if(p.z < edge) { outside = true;}

	if(p.x > dims.x-edge) { outside = true;}
	if(p.y > dims.y-edge) { outside = true;}
	if(p.z > dims.z-edge) { outside = true;}

	if(outside)
	{
		return -150.;
	} else {
		return 0.;
	}
}
'''
		
		#This function computes the velocity within a cuboid region,
		#encoded into a single 2D texture as a bunch of z-slices, like
		#so:
		#    |----------------------------------|
		#    |            z=dims.z              |
		#    |----------------------------------|
		#    |    z=dims.z*(slices-i)/slices    |
		#    |..................................|
		#    |              z=0                 |
		#    |----------------------------------|
		#
		#The resulting cuboid is sampled using position p, with (0,0,0)
		#corresponding to the bottom-left corner.
		shader += '''
vec4 getvel_cuboid(vec4 p, sampler2D field, vec3 dims, float slices)
{
	float lzp = floor(p.z/dims.z*(slices-1.))/slices;
	float hzp = ceil(p.z/dims.z*(slices-1.))/slices;
	vec3 lzv = texture2D(field, vec2(p.x/dims.x, (p.y/dims.y)/slices + lzp)).xyz;
	vec3 hzv = texture2D(field, vec2(p.x/dims.x, (p.y/dims.y)/slices + hzp)).xyz;
	float fac = (p.z/dims.z*(slices-1.) - lzp*slices);
'''
		
		#Interpolate between z-planes depending on interpolation mode.
		if self._interp_mode == "magdir":
				shader += '''
	vec3 hzvd;
	if(hzp == (slices-1.)/slices)
	{
		hzvd = normalize(lzv - vec3(0., 0., lzv.z));
	}
	else
	{
		hzvd = normalize(hzv);
	}
	vec3 lzvd = normalize(lzv.xyz);
	
	float velmag = mix(length(hzv.xyz), length(lzv.xyz), fac);
	vec3 veldir = mix(hzvd, lzvd, fac);
	vec4 vel = vec4(velmag*veldir, 0.);
'''

		elif self._interp_mode == 'linear':
			shader += "\tvec4 vel = vec4((1.-fac)*lzv + fac*hzv, 0.);\n"
		else:
			#Unsupported interpolation mode
			raise NotImplementedError
		shader += "\treturn vel;\n}\n"
			
				#This function computes velocities in local coordinates. All relevant
		#domain transformations take place here.
		shader += '''
vec4 velocity(vec4 p)
{
	'''
		#TBD: allow more complicated velocity functions.
		if self._affine:
			shader += "const vec3 dims = vec3(%e,%e,%e);\n"%(self._xw, self._yw, self._zw)
			shader += "\tvec4 v = getvel_cuboid((p)*M, vel_field, dims, %.1f);\n"%(self.texinfo['zs'])
			shader += "return v * Minv;"
		else:
			shader += "const vec3 dims = vec3(%e,%e,%e);\n"%(self._xw, self._yw, self._zw)
			shader += "\tvec4 v = getvel_cuboid(p, vel_field, dims, %.1f);\n"%(self.texinfo['zs'])
			shader += "\treturn v;\n"
		shader += "}\n"
			
		#Main function entry point
		shader += '\nvoid main(void) \n{\n'
	
		shader += '\tvec2 uv = gl_TexCoord[0].st;\n'
		
		shader += '\tconst vec2 rect = vec2(%e, %e);'%(self._rwidth, self._rheight)
		
		shader += '''
	vec4 startpos = vec4(.0001, uv*rect, 1.);
	vec4 pp = vec4(texture2D(pos, uv).xyz, 0.) + startpos;
	int i = 0;
	float nx = pp.x '''
		if self._dir == 1:
			shader += "+ "
		else:
			shader += "- "
		shader += str(self._step) + ";\n"
		#TBD: Add break conditions
		#So far, we have max iterations, passed iteration plane, and no error (.x < -100
		#corresponds to error)
		if self._dir == 1:
			shader += '\twhile(pp.x < nx && i < ' + str(self._maxiters) + ' && pp.x > -100.)\n'
		else:
			shader += '\twhile(pp.x > nx && i < ' + str(self._maxiters) + ' && pp.x > -100.)\n'
			
		shader += '\t{\n\t\ti++;\n'
		timesmat = ''
		if self._affine:
			timesmat = '*M'
		shader += "\t\tvec4 vel = velocity(pp);"
		
		#Mark particles that exit domain by placing them in an unusual position.
		if self._constrain_particles:
			shader += '''
		if(dot(vel.xyz, vel.xyz) < %e)
		{
			gl_FragColor = vec4(%e, 0., 0., 1.);
			return;
		}
	
		if(check_bounds(pp%s) == -150.)
		{
			gl_FragColor = vec4(%e, 0, 0, 1.);
			return;
		}
	'''%(self._min_vel, self._errcode['slow'], timesmat, self._errcode['exit'])
		
		#Step in direction of velocity. Ignore the magnitude.

		shader += '''
		vec4 nv = vec4(normalize(vel.xyz)*''' + str(self._itersize) + ", 0.);\n"
	
		#Either step forwards or backwards, depending on direction.
		#If this step would put us ahead of the next plane, only step as far
		#as the next plane.
		if self._dir == -1:
			shader += '''
		if(pp.x - nv.x < nx)
		{
			nv /= nv.x/(pp.x - nx);
		}
	
		pp = pp - nv;'''
		else:
			shader += '''
		if(pp.x + nv.x > nx)
		{
			nv /= nv.x/(nx - pp.x);
		}
		
		pp = pp + nv;'''
		
			#After the loop is over, store the result in the fragment color.
		shader += "\n\t}\n\tgl_FragColor = vec4(pp.xyz - startpos.xyz, 1.);\n}"
		if self._debug:
			print("OGLParticleTracer created shader:\n")
			print(shader)
			print("Compiling...")
		
		vert_shader_null = '''
		varying vec3 vertex_light_position;
		varying vec3 vertex_normal;
		
		void main()
		{
		
		    vertex_light_position = normalize(gl_LightSource[0].position.xyz);
		    
		    gl_Position = ftransform();
		    gl_TexCoord[0] = gl_MultiTexCoord0;
		    vertex_normal = normalize(gl_NormalMatrix * gl_Normal);
		}
		'''
		
		self.shaderobj = Shader([vert_shader_null], [shader])
		self.shadersrc = shader
		
		#TBD: Generalize to other channel dimensions.
		self.shaderobj.bind()
		self.shaderobj.uniformi('pos', 0)
		self.shaderobj.uniformi('vel_field', 1)
		self.shaderobj.unbind()
		self._dirty = False
		
		
	def __init__(self, *args, **kwargs):
		super(OGLParticleTracer, self).__init__(*args, **kwargs)
		
		self.filename = kwargs.get('filename', None)
		self._velocitytexture = kwargs.get('texture', None)
		
		self._edge = kwargs.get('edge_constraint', 1e-9)
		self._min_vel = kwargs.get('min_velocity', 1e-9)
		
		self._debug = kwargs.get('debug', False)
		
		self._dir = kwargs.get('direction', 1)
		self._step = np.abs(kwargs.get('stepsize', 0.01))
		self._maxiters = kwargs.get('max_iterations', 600)
		self._itersize = kwargs.get('iteration_step_size', 1./300)
		self._constrain_particles = kwargs.get('constrain_particles', True)
		self._interp_mode = kwargs.get("interpolation_mode", "linear")
		
		self._linspacenums = kwargs.get("rake_size", (800, 200))
		
		self._M = np.asmatrix(kwargs.get("affine", np.identity(4)))
		
		
		self._Minv = self._M.I
		self._affine = not np.isclose(self._M, np.identity(4)).all()
		
		self._errcode = {'exit': -150., 'slow': -200.}
		
		#Load velocity field into OpenGL texture
		if self._velocitytexture is None:
			self._velocitytexture = GLuint()
			glGenTextures(1, byref(self._velocitytexture))
		
		if self.filename is not None:
			self.texinfo = self.load(self.filename)
			
		if self._debug:
			print("Generating textures...")
		self._positiontexture, self._targettexture = GLuint(), GLuint()
		
		glGenTextures(1, byref(self._positiontexture))
		glGenTextures(1, byref(self._targettexture))
		
		#TBD: implement irregular grids
		self._regular = True
		
		#TBD: Allow different domain geometries
		self._xw = 6
		self._yw = 1
		self._zw = self.texinfo['zw']/2
		
		self._rwidth, self._rheight = kwargs.get('rect', (self._yw, self._zw))
		
		
		if self._debug:
			print("Generating framebuffer...")
		#A framebuffer we can render into.
		self.fbo = GLuint()
		glGenFramebuffersEXT(1, byref(self.fbo))
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self.fbo)
		
		#Quad we use to fill the screen
		self.renderquad = pg.graphics.Batch()
		self.renderquad.add(4, pg.gl.GL_QUADS, None, ('v2i', (0, 0, 1, 0, 1, 1, 0, 1)), ('t2f', (0, 0, 1, 0, 1, 1, 0, 1)))


		#Array of zeroes we use to initialize our textures.
		floatblack = (GLfloat * (4*self._linspacenums[0]*self._linspacenums[1]))(0)
		
		self._floatblack = floatblack
		
		#Set up texture parameters, initialize to zeroes
		if self._debug:
			print("Setting texture filtering parameters...")
		glBindTexture(GL_TEXTURE_2D, self._positiontexture)
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, self._linspacenums[0], self._linspacenums[1], 0, GL_RGBA, GL_FLOAT, floatblack)
		
		glBindTexture(GL_TEXTURE_2D, self._targettexture)
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, self._linspacenums[0], self._linspacenums[1], 0, GL_RGBA, GL_FLOAT, floatblack)
		
		if self._debug:
			print("Testing framebuffer....")
		#Make sure we have the capability to use this framebuffer
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, self._targettexture, 0)
		fb_status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT)
		if fb_status != GL_FRAMEBUFFER_COMPLETE_EXT:
			print("Framebuffer error: " + str(fb_status))

		self._dirty = True
	
	def translate(self, x=0, y=0, z=0):
		'''Translate the particles into the velocity field.
		
		This transformation occurs in the local frame of reference
		of the particles. If there is an existing rotation on the particles,
		that rotation happens _first_, such that a translation in the x direction
		always corresponds to moving the particles "forward" in their frame of reference.'''
		m = v.translation_matrix(x, y, z)
		self._affine = True
		self._dirty = True
		self._M = self._M.dot(m)
		self._Minv = self._M.I
	
	def rotate(self, theta=0, x=0, y=0, z=1):
		'''Rotate the particles about the given axis.
		
		This transformation occurs in the local frame of reference
		of the particles. If there is an existing translation on the particles,
		that translation happens _first_, such that a rotation about the x axis
		always corresponds to twisting the particles about the "forward" axis.'''
		m = v.rotation_matrix(x, y, z, theta)
		self._affine = True
		self._dirty = True
		self._M = self._M.dot(m)
		self._Minv = self._M.I
		
	def update(self, *args, **kwargs):
		pass
	
	def reset(self):
		glBindTexture(GL_TEXTURE_2D, self._positiontexture)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, 
					self._linspacenums[0], self._linspacenums[1], 
					0, GL_RGBA, GL_FLOAT, self._floatblack)
		
		glBindTexture(GL_TEXTURE_2D, self._targettexture)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F_ARB, 
					self._linspacenums[0], self._linspacenums[1], 
					0, GL_RGBA, GL_FLOAT, self._floatblack)
		
	def load(self, filename, tex = None, filetype='vectortex3d'):
		'''Load velocity field from file.
		
		As of right now, only our custom vectortex3d format is supported.'''
		
		if filetype == 'vectortex3d':
			if self._debug:
				print("Loading vectortex3d...")
			conf = ConfigParser.RawConfigParser()
			conf.read(filename)
		
			variables = conf.get("vectortex3d", "vars").split(", ")
			slicefns = conf.get("vectortex3d", "slices").split(", ")
			zs = map(float, conf.get("vectortex3d", "zs").split(", "))
			
			w = int(conf.get("vectortex3d", "width"))
			h = int(conf.get("vectortex3d", "height"))
		
			zw = float(conf.get("vectortex3d", "cheight"))
		
			assert len(zs) == len(slicefns)
			
			dirname = os.path.dirname(filename)
			
			a = array.array('f')
			for slc in slicefns:
				with open(os.path.join(dirname, slc), "rb") as f:
					d = load_floatimg(f)
					assert d[0][1] == len(variables)
					assert d[0][2] == w
					assert d[0][3] == h
					a.extend(d[1])
			
			if tex is None:
				tex = self._velocitytexture
			
			glBindTexture(GL_TEXTURE_2D, tex)
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F_ARB, w, h*len(zs), 0, GL_RGB, GL_FLOAT, a.tostring())
			d = {}
			d['spacing'] = zs[1] - zs[0]
			d['zs'] = len(zs)
			d['width'] = w
			d['height'] = h
			d['zw'] = zw
			if self._debug:
				print("Done. Parameters:")
				print(d)
			return d
		else:
			raise NotImplementedError

	def get_raw_displacements(self):
		'''Get the current displacements as a numpy array.
		
These displacements have not been post-processed, and some will be marked.'''
		#Buffer to load the image into
		output = (GLfloat * (3*self._linspacenums[0]*self._linspacenums[1]))(0)
		
		#Get pixels from GPU
		glBindTexture(GL_TEXTURE_2D, self._positiontexture)
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_FLOAT, output)
		
		#Cast into numpy array
		res = np.frombuffer(output, dtype=np.float32).reshape(self._linspacenums[1], self._linspacenums[0], 3)
		return res
	
	def get_displacements(self):
		'''Get post-processed displacements.

TBD: be smart about this, rather than just zeroing them.'''
		d = self.get_raw_displacements()
		
		#Select the particles that exited the domain
		exit_mask = self.error_mask(d, self._errcode['exit'])
		
		slow_mask = self.error_mask(d, self._errcode['slow'])
		
		#Zero these particles.
		d[exit_mask + slow_mask] = np.array((0, 0, 0))
		
		return d
	
	def error_mask(self, particles, errcode):
		return np.all(particles.reshape(self._linspacenums[0]*self._linspacenums[1], 3) == np.array((errcode, 0, 0)), axis=1).reshape(self._linspacenums[1], self._linspacenums[0])

	def step_particles(self):
		"""
		Step the particles.
		"""
		
		if self._dirty:
			self.make_shader()
		
		#Bind framebuffer where we'll render the result
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self.fbo)
		glBindTexture(GL_TEXTURE_2D, self._targettexture)
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, self._targettexture, 0)
		
		#Set projection matrix to orthographic (no perspective deformation) and set the modelview
		#matrix to have no transformations. Push the old matrices on the stack so we can restore them later.
		glMatrixMode(GL_PROJECTION)
		glPushMatrix()
		glLoadIdentity()
		glOrtho(0, 1, 0, 1, -1, 1)
		glViewport(0,0,self._linspacenums[0],self._linspacenums[1])
		glMatrixMode(GL_MODELVIEW)
		glPushMatrix()
		glLoadIdentity()
		
		#Load the location and velocity fields into texture slots 0 and 1, where the shader expects to find them.
		glActiveTexture(GL_TEXTURE0)
		glBindTexture(GL_TEXTURE_2D, self._positiontexture)
		glActiveTexture(GL_TEXTURE1)
		glBindTexture(GL_TEXTURE_2D, self._velocitytexture)
		
		#Disable blending; we don't want to use the alpha channel here.
		glDisable(GL_BLEND)
		self.shaderobj.bind()
		self.renderquad.draw()
		self.shaderobj.unbind()
		
		#Re-enable blending and reset textures so the rest of the program can use them if needed.
		glEnable(GL_BLEND)
		
		#Restore the projection and modelview matrices that we pushed onto the stack earlier.
		glPopMatrix()
		glMatrixMode(GL_PROJECTION)
		glPopMatrix()
		glMatrixMode(GL_MODELVIEW)
		
		#Return to the normal framebuffer.
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
		
		#swap textures for next pass
		self._positiontexture, self._targettexture = self._targettexture, self._positiontexture
	

