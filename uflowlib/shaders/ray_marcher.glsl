
varying vec3 vertex_normal;
varying vec3 position;
uniform sampler2D extrusion1;
uniform sampler2D extrusion2;
uniform mat3 ext2mat;


float eps = 1e-2;

vec4 backblend(vec4 front, vec4 back)
{
	front.a = clamp(front.a, 0., 1.);
	return front + back*(1.-front.a);
}

// Here we go. Into the rabbit hole...

//Distance function for a box with sides s
float box(vec3 x, vec3 s)
{
	return length(max(abs(x)-s, .0));
}

//Distance function for an infinite extrusion of a 2D distance function
float textrusion(vec3 x, sampler2D tex, vec3 dims)
{
	float d1 = texture2D(tex, x.xy/dims.xy).r;
	return max(d1, box(x - dims/2., dims/2.1));

}

//First extrusion
float ext1(vec3 x)
{
	return textrusion(x - vec3(0., .375, 0.), extrusion1, vec3(.99, .25, .99));

}

//Second extrusion
float ext2(vec3 x)
{
	return textrusion(ext2mat*vec4(x.xzy - vec3(0., .375, 0.), 1.), extrusion2, vec3(.99, .25, .99));

}

//Bounding box (keep shapes bounded within volume, for sanity)
float bbox(vec3 x)
{
	return box(x - vec3(.5), vec3(.49));
}

//Intersection of all of the above: Take the max distance.
float f (vec3 x)
{
		float e1 = ext1(x);
		float e2 = ext2(x);
		float bb = bbox(x);
		return max(max(e1, e2), bb);
}

//Compute the normal at a given point by the finite difference method.
vec3 get_normal(const vec3 p)
{
	vec3 n = vec3( f(p + vec3(eps, 0., 0.)) - f(p - vec3(eps, 0., 0.)),
				   f(p + vec3(0., eps, 0.)) - f(p - vec3(0., eps, 0.)),
				   f(p + vec3(0., 0., eps)) - f(p - vec3(0., 0., eps)));
	return normalize(n);
}


//Surface color. If we wanted to color different surfaces, we could
//sample the location, distance functions, and normals.
vec3 material(vec3 x)
{
			return vec3(.8, .1, .1);
}

//We want to bail if the ray exits the volume.
bool outside(vec3 x)
{
	return (x.x < 0. || x.y < 0. || x.z < 0. || x.x > 1. || x.y > 1. || x.z > 1.);
}

void main()
{
	//Convert screen space into local space.
	vec4 eyepos = (gl_ModelViewProjectionMatrixInverse*vec4(gl_FragCoord.xy,0.,1.));
	vec4 eyedir = (gl_ModelViewProjectionMatrixInverse*vec4(gl_FragCoord.xy,-1.,1.)) - eyepos;
	vec3 neye = normalize(eyedir.xyz);
	
	vec3 pos = position;
	int i =0;
	float minpos = 1e6;

	//Start marching in the direction of the eye ray.
	for(; i < 100 && ! outside(pos); i++)
	{
		
		//Step forward equal to the distance from the edge.
		//This guarantees we won't pass through the object.
		vec3 t = neye*f(pos);
		//Keep track of minimum distance passed from object, in case
		//we want to draw an outline.
		minpos = min(minpos, f(pos - t));
		pos -= t;
		
		if(f(pos) <= 1e-6)
		{
			//We've passed really close, let's call it.
			break;
		}
	}
	
	//Looks like this ray didn't hit anything.
	if(i == 1000 || outside(pos))
	{
			discard;
			float a =  -pow(minpos/.04, 2) + 1.;
			gl_FragColor = vec4(vec3(1., 1., 1.)*a, a);
			
		
	} else {
	
	//Color and light the surface using the phong shading model.
	vec3 col = material(pos);
	
	const vec3 light_pos = vec3( 0., 0., -1.);

	gl_FragColor = vec4(col*(max(0., dot(get_normal(pos), normalize(pos - light_pos))) + .3), 1.);
	//gl_FragColor = vec4(get_normal(pos), 1.);
	}
}